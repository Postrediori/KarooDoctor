 ############################################# 
#                                             #
#                                             #
#        KaRoo Doctor for Python 3            #
#                   v1.0                      #
#                                             #
#                                             #
#             Currently supports:             #
#                                             #
#       • Official versions (EN,FR,DE,IT,ES)  #
#       • Bootleg version (already NoCD)      #
#                                             #
 #############################################

import fnmatch
import os
import shutil
import sys
import tkinter as tk
from os.path import basename, dirname
from tkinter import filedialog

PATCH_SIGNATURES = [
    {
        "name": 'No-CD Patch',
        "signatures": [
            {
                "offset": 0x2D264,
                "orig": b'\x74\x2b',
                "new": b'\xeb\x2b'
            }
        ]
    },
    {
        "name": "Always one crystal Patch",
        "signatures": [
            {
                "offset": 0x186A6,
                "orig": b'\x5e\x5b\xc3\x90\x90\x90\x90\x90\x90\x90',
                "new": b'\xfe\x86\x06\x54\x17\x00\x5e\x5b\xc3\x90',
            },
            {
                "offset": 0x186F7,
                "orig": b'\x8b\xe9',
                "new": b'\x90\x90',
            },
            {
                "offset": 0x198A4,
                "orig": b'\x8b\xcd',
                "new": b'\x66\x47',
            },
        ]
    }
]


def patch_gamefile(file_path):
    with open(file_path, "r+b") as f:
        f.seek(0, 2)
        file_len = f.tell()

        signatures_to_patch = []
        
        # Check if file is valid and patch status
        for signature in PATCH_SIGNATURES:
            print('Checking {}...'.format(signature['name']))
            for s in signature['signatures']:
                ofs, ln = s['offset'], len(s['orig'])
                print('  Checking offset 0x{:x}... '.format(ofs), end='')
                
                # Check file is long enough
                if ofs + ln > file_len:
                    print(' ERROR File is too small for a signature')
                    return False

                # Check original contents
                f.seek(ofs)
                orig = f.read(ln)
                if orig==s['orig']:
                    signatures_to_patch.append(s)
                    print('OK')
                elif orig==s['new']:
                    print('INFO This signature is already applied')
                else:
                    print('ERROR Unknown bytes in the original file')
            
        if not signatures_to_patch:
            print('Nothing to patch in the file')
            return True

        game_exe_file = basename(file_path)
        game_dir = dirname(file_path)
    
        # Create backup file
        bak_file = os.path.join(game_dir, "{}.bak".format(game_exe_file))
        shutil.copyfile(file_path, bak_file)
        print('Copied backup of game file to {}'.format(bak_file))
        
        # Apply patched bytes
        print('Applying patches')
        for s in signatures_to_patch:
            ofs = s['offset']
            print('  Offset 0x{:x}... '.format(ofs), end='')
            f.seek(ofs)
            f.write(s['new'])
            print('OK')
            
    return True


root = tk.Tk()
root.withdraw()

# Get main game executable from command line or select with a dialog
if len(sys.argv)>1:
    file_path = sys.argv[1]
else:
    file_path = filedialog.askopenfilename(filetypes=[('Karoo.exe executable','*.exe')])
    if not file_path:
        print("WARNING No file chosen")
        sys.exit(1)

status = 0
if not patch_gamefile(file_path):
    status = 1

sys.exit(status)
