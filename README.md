## Ka'Roo Doctor

THis script fixes game files of Ka'Roo video game ([IGDB](https://www.igdb.com/games/karoo), [Twitch](https://www.twitch.tv/directory/category/karoo), [MobyGames](https://www.mobygames.com/game/7891/karoo)).

The following fixes are applied to the game executable file:
* Disable CD checks
* Start all levels with one crystal

![Karoo screenshot](karoo.jpg)

### Requirements

* Install Python 3.5 or newer

### Usage

* Launch .py file and select `Karoo.exe`
* Alternatively, start script from command line and specify path to a game file as an argument

```
KarooDoctor.py C:\Games\Karoo\Karoo.exe
```
